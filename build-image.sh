#!/usr/bin/env bash
# Usage:
#   build-image [dry-run|publish]

set -e

G_MODE="dry-run"
if [[ "${1}" = "publish" ]]; then
    G_MODE="publish"
fi

G_CHANGESET=921b45a28ab6
G_UNITY_VERSION=2021.2.9f1
G_DOCKER_IMG_VERSION=0.17

G_BUILD_IMAGE_NAME="registry.gitlab.com/shaun.burns/unity-dedicated-linux-server-docker/unityci-editor:${G_UNITY_VERSION}-linux-server-${G_DOCKER_IMG_VERSION}"

docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

docker build --tag "${G_BUILD_IMAGE_NAME}" \
  --build-arg "hubImage=unityci/hub:ubuntu-${G_DOCKER_IMG_VERSION}" \
  --build-arg "baseImage=unityci/base:ubuntu-${G_DOCKER_IMG_VERSION}" \
  --build-arg "version=${G_UNITY_VERSION}" \
  --build-arg "changeSet=${G_CHANGESET}" \
  --build-arg "module=linux-server" \
  .

if [[ "${G_MODE}" = "publish" ]]; then
    docker push "${G_BUILD_IMAGE_NAME}"
fi

docker logout "${CI_REGISTRY}"
